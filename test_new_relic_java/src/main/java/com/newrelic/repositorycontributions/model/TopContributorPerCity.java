package com.newrelic.repositorycontributions.model;

/**
 * Created by kujaomega on 13/12/17.
 */
public class TopContributorPerCity {
    private long id;
    private String contributorName;
    private int contributions;

    public TopContributorPerCity(long id, String contributorName, int contributions) {
        this.id = id;
        this.contributorName = contributorName;
        this.contributions = contributions;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }

    @Override
    public boolean equals(Object object) {

        if (object != null && object instanceof TopContributorPerCity) {
            TopContributorPerCity topContributorPerCity = (TopContributorPerCity) object;
            return topContributorPerCity.getId() == getId();
        }

        return false;
    }
}
