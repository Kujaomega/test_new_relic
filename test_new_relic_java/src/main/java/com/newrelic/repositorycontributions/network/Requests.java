package com.newrelic.repositorycontributions.network;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.newrelic.repositorycontributions.network.Response.GithubUserItem;
import com.newrelic.repositorycontributions.network.Response.Repository;
import com.newrelic.repositorycontributions.network.Response.ResponseGetUsersLocationGithub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Requests {

    private static final String TOKEN = "token 84dc4997cbbae1a218ca87dd4878e2b98f21b2bd";
    private HashMap<String, String> headers;
    private HTMLHandler htmlHandler;
    private Gson gson;

    public Requests() {
        this.headers = Maps.newHashMap();
        this.headers.put("Authorization", TOKEN);
        this.htmlHandler = new HTMLHandler();
        this.gson = new Gson();
    }

    public List<Repository> getUserRepositoriesRequest(String url){
        List<Repository> listRepository = Lists.newArrayList();
        try{
            String response = htmlHandler.doGet(url, headers);
            return gson.fromJson(response, new TypeToken<ArrayList<Repository>>(){}.getType());
        } catch (Exception e){
            e.printStackTrace();
        }
        return listRepository;
    }

    public ResponseGetUsersLocationGithub getUsersLocatedRequest(String location, int page){
        ResponseGetUsersLocationGithub responseGetUsersLocationGithub = new ResponseGetUsersLocationGithub();
        try{
            String url = String.format("https://api.github.com/search/users?q=location:%s&page=%s&per_page=100", location, page);
            String response = htmlHandler.doGet(url, headers);
            return gson.fromJson(response, ResponseGetUsersLocationGithub.class);
        } catch (Exception e){
            e.printStackTrace();
        }
        return responseGetUsersLocationGithub;
    }

    public List<GithubUserItem> getCollaboratorsRequest(String url){
        List<GithubUserItem> githubUsersItems = Lists.newArrayList();
        try{
            String response = htmlHandler.doGet(url, headers);
            return gson.fromJson(response, new TypeToken<ArrayList<GithubUserItem>>(){}.getType());
        } catch (Exception e){
            e.printStackTrace();
        }
        return githubUsersItems;
    }

}
