package com.newrelic.repositorycontributions.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by kujaomega on 14/12/17.
 */
public class HTMLHandler {
    public HTMLHandler(){}

    public String doGet(String service_url, HashMap<String,String> headers){
        try {
            URL url = new URL(service_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            for(String key:headers.keySet()){
                conn.setRequestProperty(key, headers.get(key));
            }
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException(String.format("Failed %s: HTTP error code : %s", service_url ,conn.getResponseCode()));
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String jsonMessage="";
            String output;
            while ((output = br.readLine()) != null) {
                jsonMessage = jsonMessage + output;
            }
            conn.disconnect();
            return jsonMessage;

        } catch (MalformedURLException e) {

            e.printStackTrace();
            return null;
        } catch (IOException e) {

            e.printStackTrace();
            return null;
        }
    }
}

