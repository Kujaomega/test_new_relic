package com.newrelic.repositorycontributions.network.Response;

import java.util.List;

/**
 * Created by kujaomega on 14/12/17.
 */
public class ResponseGetUsersLocationGithub {
    private int total_count;
    private boolean incomplete_results;
    private List<GithubUserItem> items;

    public ResponseGetUsersLocationGithub(){

    }

    public List<GithubUserItem> getItems() {
        return items;
    }

    public void setItems(List<GithubUserItem> items) {
        this.items = items;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public void setIncomplete_results(boolean incomplete_results) {
        this.incomplete_results = incomplete_results;
    }
}
