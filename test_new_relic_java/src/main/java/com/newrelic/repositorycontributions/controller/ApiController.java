package com.newrelic.repositorycontributions.controller;


import com.google.common.collect.Lists;
import com.newrelic.repositorycontributions.model.TopContributorPerCity;
import com.newrelic.repositorycontributions.service.TopContributorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@SpringBootApplication(scanBasePackages={"com.websystique.springboot"})
@ComponentScan(basePackages = {"com.newrelic.repositorycontributions.service"})
@RequestMapping("/api")
public class ApiController {

    @Autowired
    TopContributorsService topContributorsService;

    @RequestMapping(value="/city/{city}/{top}", method = RequestMethod.GET)
    public ResponseEntity<List<TopContributorPerCity>> listTop50TopContributors(
            @PathVariable("city") String cityName,
            @PathVariable("top") String topSize) {

        List<TopContributorPerCity> topContributorsPerCity = Lists.newArrayList();
        long startTime = System.currentTimeMillis();
        if(topSize.equals("top50")){
            topContributorsPerCity = topContributorsService.findTopContributorsPerCity(cityName, 50);
        } else if(topSize.equals("top100")){
            topContributorsPerCity = topContributorsService.findTopContributorsPerCity(cityName, 100);
        } else if(topSize.equals("top150")){
            topContributorsPerCity = topContributorsService.findTopContributorsPerCity(cityName, 150);
        } else{
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        long endTime = System.currentTimeMillis();
        long elapsedTime = endTime - startTime;
        System.out.println(String.format("Task finished in %s ms", elapsedTime));
        if (topContributorsPerCity.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<TopContributorPerCity>>(topContributorsPerCity, HttpStatus.OK);
    }
}
