package com.newrelic.repositorycontributions;

import com.newrelic.repositorycontributions.controller.ApiController;
import org.springframework.boot.SpringApplication;


public class Application {
    public static void main(String[] args) {
        SpringApplication.run(ApiController.class, args);
    }

}