package com.newrelic.repositorycontributions.service;

import com.newrelic.repositorycontributions.model.TopContributorPerCity;

import java.util.List;

/**
 * Created by kujaomega on 13/12/17.
 */
public interface TopContributorsService {
    List<TopContributorPerCity> findTopContributorsPerCity(String city, int limit);
}
