package com.newrelic.repositorycontributions.service;


import com.google.common.collect.Lists;
import com.newrelic.repositorycontributions.model.*;
import com.newrelic.repositorycontributions.network.Requests;
import com.newrelic.repositorycontributions.network.Response.GithubUserItem;
import com.newrelic.repositorycontributions.network.Response.Repository;
import com.newrelic.repositorycontributions.network.Response.ResponseGetUsersLocationGithub;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TopContributorsServiceImpl implements TopContributorsService{

    private static int NUM_OF_THREADS = 8;


    @Override
    public List<TopContributorPerCity> findTopContributorsPerCity(String city, int limit) {
        List<TopContributorPerCity> listResponseModelTopContributorsPerCity = Lists.newArrayList();
        List<GithubUserItem> usersGithub = getUsersLocatedGithub(city);
        List<Repository> cityRepositories = getUserRepositories(usersGithub);
        listResponseModelTopContributorsPerCity.addAll(getContributors(cityRepositories));
        return listResponseModelTopContributorsPerCity;
    }

    private List<Repository> getUserRepositories(List<GithubUserItem> githubUsersItems){
        List<Repository> githubRepositories = Lists.newCopyOnWriteArrayList();
        try{
            int itemsPerList = githubUsersItems.size() / NUM_OF_THREADS;
            ArrayList<Thread> threads = new ArrayList<Thread>();
            for (int i = 0; i < NUM_OF_THREADS; i++) {
                int from = (i*itemsPerList);
                int to = ((i*itemsPerList)+itemsPerList);
                if(to > githubUsersItems.size()) to = githubUsersItems.size();
                List<GithubUserItem> githubUserItemsList = githubUsersItems.subList(from, to);
                threads.add(threadGetUserRepositories(githubUserItemsList, githubRepositories));
            }

            for(Thread thread:threads){
                thread.start();
            }

            for(Thread thread: threads){
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return githubRepositories;
    }

    private List<GithubUserItem> getUsersLocatedGithub(String location){
        List<GithubUserItem> githubUsersItemList = Lists.newArrayList();
        int sizeListUsers = Integer.MAX_VALUE;
        int page = 1;
        Requests requests = new Requests();
        // The github API only allow the first 10 pages
        while (githubUsersItemList.size() < sizeListUsers && page <= 10){
            ResponseGetUsersLocationGithub usersLocationGithub = requests.getUsersLocatedRequest(location, page);
            sizeListUsers = usersLocationGithub.getTotal_count();
            githubUsersItemList.addAll(usersLocationGithub.getItems());
            page += 1;
        }
        return githubUsersItemList;
    }

    private List<TopContributorPerCity> getContributors(List<Repository> repositories){
        List<TopContributorPerCity> contributorsList = Lists.newCopyOnWriteArrayList();
        try{
            int itemsPerList = repositories.size() / NUM_OF_THREADS;
            ArrayList<Thread> threads = new ArrayList<Thread>();
            for (int i = 0; i < NUM_OF_THREADS; i++) {
                int from = (i*itemsPerList);
                int to = ((i*itemsPerList)+itemsPerList);
                if(to > repositories.size()) to = repositories.size();
                List<Repository> repositoriesItemList = repositories.subList(from, to);
                threads.add(threadGetContributors(repositoriesItemList, contributorsList));
            }

            for(Thread thread:threads){
                thread.start();
            }

            for(Thread thread: threads){
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        Collections.sort(contributorsList, new Comparator<TopContributorPerCity>() {
            @Override
            public int compare(TopContributorPerCity a1, TopContributorPerCity a2) {
                return a1.getContributions() - a2.getContributions();
            }
        }.reversed());
        return contributorsList;
    }

    private Thread threadGetContributors(List<Repository> repositories, List<TopContributorPerCity> contributorsList){
        return new Thread(){
            public void run() {
                Requests requests = new Requests();
                for(Repository repository: repositories){
                    String contributorsUrl = repository.getContributors_url();
                    List<GithubUserItem> contributorUser = requests.getCollaboratorsRequest(contributorsUrl);
                    for(GithubUserItem githubUserItem : contributorUser){
                        TopContributorPerCity topContributorPerCity = new TopContributorPerCity(githubUserItem.getId(), githubUserItem.getLogin(), 0);
                        int indexItem = contributorsList.indexOf(topContributorPerCity);
                        if(indexItem != -1){
                            contributorsList.get(indexItem).setContributions(contributorsList.get(indexItem).getContributions()+1);
                        } else{
                            contributorsList.add(topContributorPerCity);
                        }
                    }
                }
            }
        };

    }

    private Thread threadGetUserRepositories(List<GithubUserItem> githubUserItemsList, List<Repository> githubRepositories){
        return new Thread(){
            public void run() {
                Requests requests = new Requests();
                for(GithubUserItem githubUserItem : githubUserItemsList){
                    List<Repository> userRepositories = requests.getUserRepositoriesRequest(githubUserItem.getRepos_url());
                    githubRepositories.addAll(userRepositories);
                }
            }
        };
    }

}
