from django.db import models
from django.utils.timezone import now
# Create your models here.


class Contributions(models.Model):
    city = models.TextField(max_length=280)
    contributor_name = models.TextField(max_length=280)
    number_contributions = models.PositiveIntegerField(default=0)
    user_created_time = models.DateTimeField(default=now)
