__author__ = 'kujaomega'

from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from topcontributors import views


router = DefaultRouter()
router.register(r'api/city_repository_contributors_list', views.CityRepositoryContributorsListViewSet, 'city_repository_contributors_list')
router.register(r'api/city/(?P<city>.+)/top_50', views.Top50CityRepositoryContributorsListViewSet, 'city50')
router.register(r'api/city/(?P<city>.+)/top_100', views.Top100CityRepositoryContributorsListViewSet, 'city100')
router.register(r'api/city/(?P<city>.+)/top_150', views.Top150CityRepositoryContributorsListViewSet, 'city150')


urlpatterns = [
    url(r'^', include(router.urls)),
]
