from django.contrib import admin
from topcontributors import models
# Register your models here.


class ContributionsAdmin(admin.ModelAdmin):
    list_display = ['id', 'city', 'contributor_name', 'number_contributions', 'user_created_time']
    ordering = ['-id']


admin.site.register(models.Contributions, ContributionsAdmin)
