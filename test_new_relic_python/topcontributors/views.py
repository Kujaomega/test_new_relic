from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from topcontributors import models, serializers
from topcontributors.viewsets import DebugViewSet
from topcontributors.permissions import IsUserOnly
from rest_framework.pagination import PageNumberPagination
# Create your views here.


class RepositoryContributorsPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 50


class CityRepositoryContributorsListViewSet(DebugViewSet):
    queryset = models.Contributions.objects.all()
    serializer_class = serializers.DebugCityRepositoryContributorsListSerializer
    permission_classes = (AllowAny, AllowAny)
    lookup_field = 'id'
    pagination_class = RepositoryContributorsPagination


class Top50CityRepositoryContributorsListViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Contributions.objects.all()
    serializer_class = serializers.TopCityRepositoryContributorsListSerializer
    permission_classes = (IsUserOnly,)
    lookup_field = 'id'
    pagination_class = RepositoryContributorsPagination

    def get_queryset(self):
        city = self.kwargs['city']
        repositories_contributions = models.Contributions.objects.filter(city=city).order_by('number_contributions').reverse()[:50]
        return repositories_contributions


class Top100CityRepositoryContributorsListViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Contributions.objects.all()
    serializer_class = serializers.TopCityRepositoryContributorsListSerializer
    permission_classes = (IsUserOnly,)
    lookup_field = 'id'
    pagination_class = RepositoryContributorsPagination

    def get_queryset(self):
        city = self.kwargs['city']
        repositories_contributions = models.Contributions.objects.filter(city=city).order_by('number_contributions').reverse()[:100]
        return repositories_contributions


class Top150CityRepositoryContributorsListViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Contributions.objects.all()
    serializer_class = serializers.TopCityRepositoryContributorsListSerializer
    permission_classes = (IsUserOnly,)
    lookup_field = 'id'
    pagination_class = RepositoryContributorsPagination

    def get_queryset(self):
        city = self.kwargs['city']
        repositories_contributions = models.Contributions.objects.filter(city=city).order_by('number_contributions').reverse()[:150]
        return repositories_contributions
