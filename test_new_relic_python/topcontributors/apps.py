from django.apps import AppConfig


class TopcontributorsConfig(AppConfig):
    name = 'topcontributors'
