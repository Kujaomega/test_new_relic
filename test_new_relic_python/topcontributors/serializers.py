from rest_framework import serializers
from topcontributors import models


class DebugCityRepositoryContributorsListSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Contributions
        fields = '__all__'


class TopCityRepositoryContributorsListSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Contributions
        fields = ('contributor_name', 'number_contributions')
