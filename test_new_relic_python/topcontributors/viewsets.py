from rest_framework import mixins, viewsets



class DebugViewSet(
  mixins.ListModelMixin,
  mixins.CreateModelMixin,
  mixins.UpdateModelMixin,
  mixins.DestroyModelMixin,
  viewsets.GenericViewSet):
    pass
