New Relic - Coding challenge

The correct service is inside the folder test_new_relic_java. The service returns the top contributors for a city. This service works for small cities of the world. The Github API of Search is limited to the firsts 10 pages and there are also problems with throttling (https://developer.github.com/v3/rate_limit/) if the city is big. Small tested cities: Olot, Albacete

Java Service:
	Requirements:
		- Maven
		- Docker
		- Bash
	The service is tested in ubuntu 16.04, to run It you have to run the bash file "build_run.sh".


	The endpoints are the following:
		http://localhost/api/city/<city>/top50
		http://localhost/api/city/<city>/top100
		http://localhost/api/city/<city>/top150
